pub mod prelude;
pub mod error;

use std::{path::Path, fs::File, io::{BufReader, Read}};

use indexmap::IndexMap;

pub fn extract_raw_from_path(path: impl AsRef<Path>) -> Option<RawParameters> {
    let file = BufReader::new(File::open(path).unwrap());
    extract_raw_from_reader(file)
}

pub fn extract_raw_from_reader(data: impl Read) -> Option<RawParameters> {
    // if is PNG
    png(data)

    // if is JPEG

    // if is WebP
}

fn _exif(path: &str) {
    let file = std::fs::File::open(path).unwrap();
    let mut bufreader = std::io::BufReader::new(&file);
    let exifreader = exif::Reader::new();
    let exif = exifreader.read_from_container(&mut bufreader).unwrap();
    for f in exif.fields() {
        println!("{} {} {}",
            f.tag, f.ifd_num, f.display_value().with_unit(&exif));
    }
}

fn png(data: impl Read) -> Option<RawParameters> {
    let decoder = png::Decoder::new(data);
    let reader = decoder.read_info().unwrap();

    for chunk in &reader.info().uncompressed_latin1_text {
        if chunk.keyword == "parameters" {
            return Some(RawParameters(chunk.text.clone()));
        }

        // println!("{}: {}", chunk.keyword, chunk.text);
    }

    None
}

#[derive(Clone, Debug)]
pub struct RawParameters(String);

impl RawParameters {
    pub fn parse(&self) -> Parameters {
        Self::do_parse(self.0.clone())
    }

    pub fn into_parsed(self) -> Parameters {
        Self::do_parse(self.0)
    }

    fn do_parse(raw: String) -> Parameters {
        let (prompt, other) = raw.rsplit_once('\n').unwrap_or(("", &raw));
        let (prompt, negative) = prompt.split_once("\nNegative prompt: ").unwrap_or((prompt, ""));

        let prompt = prompt.replace('\n', "␤");
        let negative = negative.replace('\n', "␤");

        // println!("Prompt:   {}", prompt);
        // println!("Negative: {}", negative);
        // println!("Other:    {} ", other);

        let mut steps: Option<u32> = None;
        let mut sampler: Option<String> = None;
        let mut cfg_scale: Option<f32> = None;
        let mut seed: Option<u32> = None;
        let mut size: Option<String> = None;
        let mut model_hash: Option<String> = None;
        let mut model_name: Option<String> = None;
        let mut clip_skip: Option<u8> = None;
        let mut version: Option<String> = None;

        for info in other.split(',') {
            let (name, value) = info.split_once(':').unwrap();
            let name = name.trim_start();
            let value = value.trim_start();

            // println!("{}: {}", name, value);

            /*
             * Steps: 20
             * Sampler: Euler a
             * CFG scale: 7
             * Seed: 3617184263
             * Size: 512x512
             * Model hash: 8340e74c3e
             * Model: fantassifiedIcons_fantassifiedIconsV20
             * Clip skip: 2
             * Version: v1.5.1
            */

            match name {
                "Steps" => {steps = Some(skip_fail!(value.parse()))},
                "Sampler" => {sampler = Some(value.to_string())},
                "CFG scale" => {cfg_scale = Some(skip_fail!(value.parse()))},
                "Seed" => {seed = Some(skip_fail!(value.parse()))},
                "Size" => {size = Some(value.to_string())},
                "Model hash" => {model_hash = Some(value.to_string())},
                "Model" => {model_name = Some(value.to_string())},
                "Clip skip" => {clip_skip = Some(skip_fail!(value.parse()))},
                "Version" => {version = Some(value.to_string())},
                _ => {},
            };
        };

        Parameters {
            raw,
            prompt,
            negative,
            steps,
            sampler,
            cfg_scale,
            seed,
            size,
            model_hash,
            model_name,
            clip_skip,
            version,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Parameters {
    pub raw: String,
    pub prompt: String,
    pub negative: String,
    pub steps: Option<u32>,
    pub sampler: Option<String>,
    pub cfg_scale: Option<f32>,
    pub seed: Option<u32>,
    pub size: Option<String>,
    pub model_hash: Option<String>,
    pub model_name: Option<String>,
    pub clip_skip: Option<u8>,
    pub version: Option<String>,
}

impl Parameters {
    pub fn key_value_pairs(&self) -> IndexMap<&str, String> {
        let mut map = IndexMap::new();
        // map.insert("Prompt", self.prompt.clone());

        if let Some(value) = self.seed {
            map.insert("Seed", value.to_string());
        }

        if let Some(value) = self.steps {
            map.insert("Steps", value.to_string());
        }

        if let Some(value) = &self.size {
            map.insert("Size", value.clone());
        }

        if let Some(value) = &self.model_name {
            map.insert("Model Name", value.clone());
        }

        if let Some(value) = &self.model_hash {
            map.insert("Model Hash", value.clone());
        }

        if let Some(value) = &self.sampler {
            map.insert("Sampler", value.clone());
        }

        if let Some(value) = self.cfg_scale {
            map.insert("CFG Scale", value.to_string());
        }

        if let Some(value) = self.clip_skip {
            map.insert("Clip Skip", value.to_string());
        }

        if let Some(value) = &self.version {
            map.insert("Version", value.clone());
        }

        map
    }
}

#[macro_export]
macro_rules! skip_fail {
    ($res:expr) => {
        match $res {
            Ok(val) => val,
            Err(e) => {
                eprintln!("{e}");
                continue;
            }
        }
    };
}
