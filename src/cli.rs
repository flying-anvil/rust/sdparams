use std::process::exit;

use clap::Parser;

use self::extract_command::{ExtractCommand, Format};

mod extract_command;

#[derive(Parser, Debug)]
#[command(propagate_version = true)]
#[command(author, version, about, long_about = None)]
#[command(disable_colored_help = false)]
#[command(
    // help_template = "{author-with-newline} {about-section}Version: {version} \n {usage-heading} {usage} \n {all-args} {tab}"
    help_template = "{usage-heading} {usage} \n\n{all-args}{tab}\n\n\x1B[1;4mAbout:\x1B[0m\n\x1B[3m  Authors: {author-with-newline}  Version: {version}\x1B[0m"
)]
struct Cli {
    /// File(s) to extract from
    files: Vec<String>,

    /// Format of the output
    #[arg(value_enum, short, long)]
    format: Option<Format>,
}

pub(crate) fn run() {
    let args = Cli::parse();

    #[cfg(debug_assertions)]
    println!("{args:#?}");

    let command = ExtractCommand::new(args.files, args.format);
    if let Err(error) = command.run() {
        eprintln!("{error}");
        exit(1);
    }
}
