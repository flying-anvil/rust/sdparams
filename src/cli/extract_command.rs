use std::{path::Path, process::exit};

use clap::{Args, ValueEnum};

use sdparams::{prelude::Result, extract_raw_from_path, Parameters};

#[derive(Args, Clone, Debug)]
pub struct ExtractCommand {
    /// File(s) to extract from
    files: Vec<String>,

    /// Format of the output
    #[arg(value_enum, short, long)]
    format: Option<Format>,
}

impl ExtractCommand {
    pub fn new(files: Vec<String>, format: Option<Format>) -> Self { Self { files, format } }

    pub(crate) fn run(&self) -> Result<()> {
        if self.files.is_empty() {
            eprintln!("No files given");
            exit(1);
        }

        let format = self.format.unwrap_or_default();

        for path in &self.files {
            let path = Path::new(&path);
            let filename = path.file_name().unwrap().to_str().unwrap();

            if !path.exists() {
                eprintln!("{} does not exist", &path.to_str().unwrap());
                continue;
            }

            if path.is_dir() {
                eprintln!("{} is a directory", &path.to_str().unwrap());
                continue;
            }

            let raw = match extract_raw_from_path(path) {
                None => {
                    println!("{}", filename);
                    println!("\x1B[3mNo parameters found\x1B[0m");
                    println!();
                    continue;
                },
                Some(value) => value,
            };

            let parsed = raw.into_parsed();
            print_parameters(&parsed, filename, format);
        }

        Ok(())
    }
}

#[derive(ValueEnum, Default, Clone, Copy, Debug)]
pub enum Format {
    Plain,
    #[default]
    Pretty,
    Json,
    Csv,
}

fn print_parameters(parameters: &Parameters, filename: &str, format: Format) {
    match format {
        Format::Plain  => print_parameters_plain(parameters, filename),
        Format::Pretty => print_parameters_pretty(parameters, filename),
        Format::Json => todo!(),
        Format::Csv => todo!(),
    }
}

fn print_parameters_plain(parameters: &Parameters, filename: &str) {
    println!("{}", filename);
    println!("Prompt: {}", parameters.prompt);
    println!("Steps: {}", match parameters.seed {
        Some(value) => value.to_string(),
        None => "?".to_string(),
    });
    println!("Steps: {}", match parameters.steps {
        Some(value) => value.to_string(),
        None => "?".to_string(),
    });
    println!("Size: {}", match &parameters.size {
        Some(value) => value,
        None => "?",
    });
    println!("Model: {}", match &parameters.model_name {
        Some(value) => value,
        None => "?",
    });
    println!("Model Hash: {}", match &parameters.model_hash {
        Some(value) => value.to_string(),
        None => "?".to_string(),
    });
    println!("Sampler: {}", match &parameters.sampler {
        Some(value) => value,
        None => "?",
    });
    println!("CFG Scale: {}", match parameters.cfg_scale {
        Some(value) => value.to_string(),
        None => "?".to_string(),
    });
    println!("Clip Skip: {}", match &parameters.clip_skip {
        Some(value) => value.to_string(),
        None => "?".to_string(),
    });
    println!("Version: {}", match &parameters.version {
        Some(value) => value,
        None => "?",
    });
    println!();
}

fn print_parameters_pretty(parameters: &Parameters, filename: &str) {
    const MAX_LINE_LENGTH: usize = 140;

    let pairs = parameters.key_value_pairs();

    let longest_key = pairs.keys().map(|key| key.chars().count()).max().unwrap_or(16);
    let longest_value = pairs.values().map(|key| key.chars().count()).max().unwrap_or(16)
        .max(parameters.prompt.chars().count().saturating_sub(longest_key + 3))
        .max(parameters.negative.chars().count().saturating_sub(longest_key + 3))
        .min(MAX_LINE_LENGTH - longest_key - 7);

    // TODO: Split too long line into multiple ones
//    let max_prompt_line_width = longest_key + longest_value + 3;
//    println!("{max_prompt_line_width}");
//    let mut prompt_lines = Vec::new();
//    let mut current_line = String::with_capacity(max_prompt_line_width);
//    for word in parameters.prompt.split(",") {
//        if current_line.chars().count() + word.chars().count() + 1 > max_prompt_line_width {
//            prompt_lines.push(current_line.clone());
//            current_line.clear();
//
//            current_line.push_str(word.trim_start());
//            continue;
//        }
//
//        if !current_line.is_empty() {
//            current_line.push_str(",");
//        }
//
//        current_line.push_str(word);
//    }
//
//    println!("{:#?}", prompt_lines);

    let filename_length = filename.chars().count();
    let top_total_length = longest_key + longest_value;

    // The filename might be shorter, longer or equal in size to the rest of the table, so the separator needs to adjust.
    let filename_separator_line = if (top_total_length + 3) > filename_length {
        format!("├─{}─┴─{}─╮", "─".repeat(filename_length), "─".repeat(top_total_length - filename_length))
    } else if (top_total_length + 3) < filename_length {
        format!("├─{}─┬─{}─╯", "─".repeat(top_total_length + 3), "─".repeat(filename_length - top_total_length - 6))
    } else {
        format!("├─{}─┤", "─".repeat(filename_length))
    };

    println!("╭─{}─╮", "─".repeat(filename_length));
    println!("│ {} │", filename);
    println!("{filename_separator_line}");
    //println!("├─{}─{}{}{}", "─".repeat(filename_length), filename_separator_a, "─".repeat(padding_post), filename_separator_b);
    println!("│ \x1B[1m{:^padding$}\x1B[0m │", parameters.prompt, padding = longest_key + longest_value + 3);

    if !parameters.negative.is_empty() {
        println!("├─{}───{}─┤", "─".repeat(longest_key), "─".repeat(longest_value));
        println!("│ \x1B[2m{:^padding$}\x1B[0m │", parameters.negative, padding = longest_key + longest_value + 3);
    }

    println!("├─{}─┬─{}─┤", "─".repeat(longest_key), "─".repeat(longest_value));

    // pairs.keys().sorted().for_each(|key| {
    pairs.iter().for_each(|(key, value)| {
        println!("│ {: >longest_key$} │ {: <longest_value$} │", key, value);
    });
    println!("╰─{}─┴─{}─╯", "─".repeat(longest_key), "─".repeat(longest_value));
}

fn _ucfirst(input: &str) -> String {
    if input.is_empty() {
        return "".to_string();
    }

    let first = match input.chars().next() {
        None => return "".to_string(),
        Some(char) => char,
    }.to_string();

    let rest = input.chars().skip(1).collect::<String>();
    let mut result = String::with_capacity(input.len() + 1); // Some letters will be two chars when uppercased (ß)

    result.push_str(&first.to_uppercase());
    result.push_str(&rest);

    result
}
